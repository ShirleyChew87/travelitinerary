//
//  AddTripViewController.swift
//  TravelItinerary
//
//  Created by Wong Ding Jian on 16/07/2016.
//  Copyright © 2016 ShirleyChew. All rights reserved.
//

import UIKit
import MBProgressHUD

class AddTripViewController: UIViewController {
    
    var trip: Trip?
    var tripAddedBlock: ((trip: Trip) -> Void)?
    var attractionsAddedBlock: ((attractions: [Attraction]) -> Void)?
    var startDatePickerView: UIDatePicker = UIDatePicker()
    var endDatePickerView: UIDatePicker = UIDatePicker()
    let cityPickerView = UIPickerView()
    var availableCountries: [String] = []
    var availableCities: [String] = []
    var selectedCountry: String?
    var selectedCity: String?
    var selectedCountryIndex: Int = 0
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var createItineraryButton: UIBarButtonItem!
    @IBOutlet weak var startDateTextField: UITextField!
    @IBOutlet weak var endDateTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var completeAllFieldWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var endDateErrorWidthConstraint: NSLayoutConstraint!
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var loadingLabel: UILabel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.cityPickerView.dataSource = self
        self.cityPickerView.delegate = self
        
        //Load country listing from API
        var loadCountriesTask: NSURLSessionDataTask?
        loadCountriesTask = TripLoader.sharedLoader.readCountryFromAPI({ (availableCountries, availableCities) in
            self.availableCountries = availableCountries
            self.availableCities = availableCities
            self.cityPickerView.reloadAllComponents()
        })
        loadCountriesTask?.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func startDateTextFieldAction(sender: UITextField) {
        startDatePickerView.datePickerMode = UIDatePickerMode.DateAndTime
        startDatePickerView.minuteInterval = 30
        sender.inputView = startDatePickerView
        
        startDatePickerView.addTarget(self, action: #selector(AddTripViewController.datePickerValueChanged), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    @IBAction func endDateTextFieldAction(sender: UITextField) {
        //Sets the end date to the start date initially as end date is usually later than start date
        endDatePickerView.date = startDatePickerView.date
        endDatePickerView.datePickerMode = UIDatePickerMode.DateAndTime
        endDatePickerView.minuteInterval = 30
        sender.inputView = endDatePickerView
        
        endDatePickerView.addTarget(self, action: #selector(AddTripViewController.datePickerValueChanged), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = .ShortStyle
        
        if sender == startDatePickerView {
            startDateTextField.text = dateFormatter.stringFromDate(sender.date)
        } else if sender == endDatePickerView {
            //Ensure end date selected is later than start date
            if startDatePickerView.date.compare(endDatePickerView.date) == .OrderedDescending {
                endDatePickerView.date = startDatePickerView.date
                endDateErrorWidthConstraint.constant = 130
            } else {
                endDateErrorWidthConstraint.constant = 0
                endDateTextField.text = dateFormatter.stringFromDate(sender.date)
            }
        }
    }
    
    @IBAction func cityTextFieldAction(sender: UITextField) {
        sender.inputView = self.cityPickerView
    }
    
    @IBAction func createItinerary(sender: AnyObject) {
        
        //Add Attractions to Selected City and create Trip
        if let selectedCountry = self.selectedCountry {
            self.trip = Trip(country: selectedCountry, city: self.selectedCity!, startDate: startDatePickerView.date, endDate: endDatePickerView.date)
            
            if let _ = self.trip {
                let hud: MBProgressHUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                
                var loadAttractionsTask: NSURLSessionDataTask?
                loadAttractionsTask = TripLoader.sharedLoader.readAttractionsFromAPI(self.selectedCountry!, city: self.selectedCity!, completionBlock: { (availableAttractions) in
                    hud.hideAnimated(true)
                    self.trip?.cityAttractions = availableAttractions
                    self.tripAddedBlock?(trip: self.trip!)
                })
                loadAttractionsTask?.resume()
            }
        } else {
            self.completeAllFieldWidthConstraint.constant = 200
        }
    }
    
    @IBAction func cancelButtonAction(sender: AnyObject) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion:
            nil)
    }
}

extension AddTripViewController: UIPickerViewDataSource {

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var rowsInComponentCount: Int = 0
        if component == 0 {
            rowsInComponentCount = self.availableCountries.count
        } else {
            //Upon loading countries from API, load cities for first country in the list
            if self.availableCountries.count > 0 {
                rowsInComponentCount = citiesForSelectedCountry(0).count
            }
            //If country is selected, reload cities to reflect cities in that country
            if let _ = self.selectedCountry {
                rowsInComponentCount = citiesForSelectedCountry(self.selectedCountryIndex).count
            }
        }
        return rowsInComponentCount
    }
}

extension AddTripViewController: UIPickerViewDelegate {
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var dataToDisplayInRow: String = ""
        //If there are countries loaded from API, display countries and cities for the first country in the list
        if self.availableCountries.count > 0 {
            if component == 0 {
                dataToDisplayInRow = self.availableCountries[row]
            } else {
                if self.selectedCountry != nil {
                    dataToDisplayInRow = citiesForSelectedCountry(self.selectedCountryIndex)[row]
                    updateTextField(self.selectedCountry!, selectedCity: citiesForSelectedCountry(self.selectedCountryIndex)[0])
                } else {
                //If no country was selected in Column 1, set selected Country to first Country and add Cities for display in Column 2. Upon display, update text field to first Country and first City.
                self.selectedCountry = self.availableCountries[0]
                dataToDisplayInRow = citiesForSelectedCountry(self.selectedCountryIndex)[row]
                self.selectedCity = citiesForSelectedCountry(self.selectedCountryIndex)[0]
                updateTextField(self.selectedCountry!, selectedCity: self.selectedCity!)
                }
            }
        }
        return dataToDisplayInRow
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            if self.availableCountries.count > 0 {
                self.selectedCountry = self.availableCountries[row]
                self.selectedCountryIndex = row
                pickerView.reloadComponent(1) //update City column based on Country Selected
                pickerView.selectRow(0, inComponent: 1, animated: false) //reset City selection
                updateTextField(self.selectedCountry!, selectedCity: citiesForSelectedCountry(self.selectedCountryIndex)[0])
            }
        } else {
            if let selectedCountry = self.selectedCountry {
                self.selectedCity = citiesForSelectedCountry(self.selectedCountryIndex)[row]
                updateTextField(selectedCountry, selectedCity: self.selectedCity!)
            }
        }
    }
    
    func citiesForSelectedCountry(selectedCountryIndex: Int) -> [String] {
        let cities: String = self.availableCities[selectedCountryIndex]
        let citiesArray: [String] = cities.componentsSeparatedByString(",")
        
        return citiesArray
    }
    
    func updateTextField(selectedCountry: String, selectedCity: String) {
        self.cityTextField.text = "\(selectedCity), \(selectedCountry)"
    }
}