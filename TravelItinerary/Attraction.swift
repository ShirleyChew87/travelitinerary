//
//  Attraction.swift
//  TravelItinerary
//
//  Created by Wong Ding Jian on 19/07/2016.
//  Copyright © 2016 ShirleyChew. All rights reserved.
//

import Foundation
import UIKit

class Attraction: NSObject, NSCoding {
    var name: String!
    var country: String?
    var city: String?
    var address: String? //Consider using map
    var writeUp: String?
    var openingHours: String?
    var fee: String?
    var photoURLString: String?
    var photo: UIImage?
    var reviews: String?
    var id: String?
    
    init(name: String) {
        self.name = name
        super.init()
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        // Read saved data
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver

        let name: String = keyedUnarchiver.decodeObjectForKey("MTIAttractionName") as! String
        
        self.init(name: name)
        
        if let country: String = keyedUnarchiver.decodeObjectForKey("MTIAttractionCountry") as? String {
            self.country = country
        }
        if let city: String = keyedUnarchiver.decodeObjectForKey("MTIAttractionCity") as? String {
            self.city = city
        }
        if let address: String = keyedUnarchiver.decodeObjectForKey("MTIAttractionAddress") as? String {
            self.address = address
        }
        if let writeUp: String = keyedUnarchiver.decodeObjectForKey("MTIAttractionWriteUp") as? String {
            self.writeUp = writeUp
        }
        if let openingHours: String = keyedUnarchiver.decodeObjectForKey("MTIAttractionOpeningHours") as? String {
            self.openingHours = openingHours
        }
        if let fee: String = keyedUnarchiver.decodeObjectForKey("MTIAttractionFee") as? String {
            self.fee = fee
        }
        if let photoURLString: String = keyedUnarchiver.decodeObjectForKey("MTIAttractionPhotoURL") as? String {
            self.photoURLString = photoURLString
        }
        if let photo: UIImage = keyedUnarchiver.decodeObjectForKey("MTIAttractionPhoto") as? UIImage {
            self.photo = photo
        }
        if let reviews: String = keyedUnarchiver.decodeObjectForKey("MTIAttractionReviews") as? String {
            self.reviews = reviews
        }
        if let id: String = keyedUnarchiver.decodeObjectForKey("MTIAttractionId") as? String {
            self.id = id
        }
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver

        keyedArchiver.encodeObject(self.name, forKey: "MTIAttractionName")
        keyedArchiver.encodeObject(self.country, forKey: "MTIAttractionCountry")
        keyedArchiver.encodeObject(self.city, forKey: "MTIAttractionCity")
        keyedArchiver.encodeObject(self.address, forKey: "MTIAttractionAddress")
        keyedArchiver.encodeObject(self.writeUp, forKey: "MTIAttractionWriteUp")
        keyedArchiver.encodeObject(self.openingHours, forKey: "MTIAttractionOpeningHours")
        keyedArchiver.encodeObject(self.fee, forKey: "MTIAttractionFee")
        keyedArchiver.encodeObject(self.photoURLString, forKey: "MTIAttractionPhotoURL")
        keyedArchiver.encodeObject(self.photo, forKey: "MTIAttractionPhoto")
        keyedArchiver.encodeObject(self.reviews, forKey: "MTIAttractionReviews")
        keyedArchiver.encodeObject(self.id, forKey: "MTIAttractionId")
    }

//    func averageRating() -> Double {
//        if reviews.count == 0 {
//            return 0.0
//        }
//        
//        var average: Double = 0.0
//        for review in self.reviews {
//            average += Double(review.rating)
//        }
//        return average / Double(reviews.count)
//    }
    
    func jsonDict() -> [String : AnyObject] {
        var dict: [String : AnyObject] = [:]
        dict["___class"] = "Attractions"
        dict["name"] = self.name
        dict["country"] = self.country
        dict["city"] = self.city
        dict["address"] = self.address
        dict["writeUp"] = self.writeUp
        dict["openingHours"] = self.openingHours
        dict["fee"] = self.fee
        dict["photo"] = self.photoURLString
        dict["reviews"] = self.reviews
        dict["objectId"] = self.id

        return dict
    }
}