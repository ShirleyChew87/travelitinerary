//
//  AttractionCell.swift
//  TravelItinerary
//
//  Created by Wong Ding Jian on 08/08/2016.
//  Copyright © 2016 ShirleyChew. All rights reserved.
//

import UIKit

class AttractionCell: UITableViewCell {

    @IBOutlet weak var infoLabel: UILabel!
    var attraction: Attraction!

    var section: Int! {
        didSet {
            updateDisplay()
        }
    }
    
    func updateDisplay() {
        
        switch section {
        case 0:
            self.infoLabel.text = attraction.writeUp
        case 1:
            self.infoLabel.text = attraction.address
        case 2:
            self.infoLabel.text = attraction.openingHours
        case 3:
            self.infoLabel.text = attraction.fee
        case 4:
            self.infoLabel.text = attraction.reviews
        default:
            self.infoLabel.text = "Sections more than required"
        }
    }

}
