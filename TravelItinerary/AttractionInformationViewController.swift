//
//  AttractionInformationViewController.swift
//  TravelItinerary
//
//  Created by Wong Ding Jian on 21/07/2016.
//  Copyright © 2016 ShirleyChew. All rights reserved.
//

import UIKit

class AttractionInformationViewController: UIViewController {

    var attraction: Attraction!
    @IBOutlet weak var informationTableView: UITableView!
    @IBOutlet weak var attractionImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        self.informationTableView.dataSource = self
        self.informationTableView.delegate = self
        self.informationTableView.rowHeight = UITableViewAutomaticDimension
        self.informationTableView.estimatedRowHeight = 100
        
        if let photo = attraction.photo {
            self.attractionImage.image = photo
        } else {
            if let photoURL = attraction.photoURLString {
                var loadAttractionImageTask: NSURLSessionDataTask?
                loadAttractionImageTask = TripLoader.sharedLoader.loadAttractionImageFromAPI(photoURL, completionBlock: {(photo) in
                    self.attraction.photo = photo
                    self.attractionImage.image = photo
                })
                loadAttractionImageTask?.resume()
            }
        }

        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AttractionInformationViewController: UITableViewDataSource, UITableViewDelegate {
 
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 5 // Name & Description, Address & Location, Opening Hour, Fee, Reviews
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return attraction.name
        case 1:
            return "Address & Location"
        case 2:
            return "Opening Hours"
        case 3:
            return "Fee"
        case 4:
            return "Rating & Reviews"
        default:
            return "Sections more than required"
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: AttractionCell = tableView.dequeueReusableCellWithIdentifier("AttractionCell", forIndexPath: indexPath) as! AttractionCell
        
        cell.attraction = self.attraction
        cell.section = indexPath.section
        
        return cell
    }
}