//
//  Day.swift
//  TravelItinerary
//
//  Created by Wong Ding Jian on 11/08/2016.
//  Copyright © 2016 ShirleyChew. All rights reserved.
//

import Foundation

class Day: NSObject, NSCoding {
    var itineraries: [Itinerary] = []
    var date: NSDate!
    
    func encodeWithCoder(aCoder: NSCoder) {
        //Encode and archiver data
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        keyedArchiver.encodeObject(self.date, forKey: "MTIDayDate")
        keyedArchiver.encodeObject(self.itineraries, forKey: "MTIDayItineraries")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        //Read saved data
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        let date: NSDate = keyedUnarchiver.decodeObjectForKey("MTIDayDate") as! NSDate
        
        //Create Day instance, passing it the saved data
        self.init(date: date)
        
        //Load saved itineraries of the day to the new instance
        let savedItineraries: [Itinerary] = keyedUnarchiver.decodeObjectForKey("MTIDayItineraries") as! [Itinerary]
        self.itineraries += savedItineraries
    }
    
    init(date: NSDate!) {
        self.date = date
        
        super.init()
    }
    
    func jsonDict() -> [ String : AnyObject ] {
        var itineraryDictArray: [[String : AnyObject]] = []
        for itinerary in self.itineraries {
            itineraryDictArray.append(itinerary.jsonDict())
        }
 
        let dayDict : [String : AnyObject] = [
            "___class": "Days",
            "dayDate" : TripLoader.sharedLoader.convertNSDateToString(self.date),
            "itineraries" : itineraryDictArray
        ]

        return dayDict
    }
}