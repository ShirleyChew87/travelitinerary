//
//  Itinerary.swift
//  TravelItinerary
//
//  Created by Wong Ding Jian on 19/07/2016.
//  Copyright © 2016 ShirleyChew. All rights reserved.
//

import Foundation

class Itinerary: NSObject, NSCoding {
    var attraction: Attraction!
    var startTime: NSDate!
    var endTime: NSDate?
    
    init(attraction: Attraction, startTime: NSDate? = NSDate()) {
        self.attraction = attraction
        self.startTime = startTime
        
        super.init()
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver

        keyedArchiver.encodeObject(self.attraction, forKey: "MTIItineraryAttraction")
        keyedArchiver.encodeObject(self.startTime, forKey: "MTIItineraryStartTime")
        keyedArchiver.encodeObject(self.endTime, forKey: "MTIItineraryEndTime")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        // Read saved data
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver

        let attraction: Attraction = keyedUnarchiver.decodeObjectForKey("MTIItineraryAttraction") as! Attraction
        let startTime: NSDate = keyedUnarchiver.decodeObjectForKey("MTIItineraryStartTime") as! NSDate
        
        self.init(attraction: attraction, startTime: startTime)
        
        if let endTime: NSDate = keyedUnarchiver.decodeObjectForKey("MTIItineraryEndTime") as? NSDate {
            self.endTime = endTime
        }
    }
    
    func jsonDict() -> [ String : AnyObject ] {
        let itineraryDict : [ String : AnyObject ] = [
            "___class": "Itineraries",
            "startTime" : TripLoader.sharedLoader.convertNSDateToString(self.startTime),
            "endTime" : NSNull(),
            "attraction1" : self.attraction.jsonDict()
        ]

        return itineraryDict
    }
}