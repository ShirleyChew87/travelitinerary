//
//  ItineraryCell.swift
//  TravelItinerary
//
//  Created by Rizuwan Zulkifli on 24/07/2016.
//  Copyright © 2016 ShirleyChew. All rights reserved.
//

import UIKit

class ItineraryCell: UITableViewCell {

    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!

    
    override func awakeFromNib() {
                super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
