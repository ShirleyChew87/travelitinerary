//
//  MyItineraryCell.swift
//  TravelItinerary
//
//  Created by Wong Ding Jian on 26/07/2016.
//  Copyright © 2016 ShirleyChew. All rights reserved.
//

import UIKit

class MyItineraryCell: UITableViewCell {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    
    @IBOutlet weak var endDateLabel: UILabel!
    
    var trip: Trip! {
        didSet {
            updateDisplay()
        }
    }
    
    func updateDisplay() {
        
        self.cityLabel.text = trip.city
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        
        self.startDateLabel.text = dateFormatter.stringFromDate(trip.startDate)
        self.endDateLabel.text = dateFormatter.stringFromDate(trip.endDate)
    }
}
