//
//  MyItineraryViewController.swift
//  TravelItinerary
//
//  Created by Wong Ding Jian on 26/07/2016.
//  Copyright © 2016 ShirleyChew. All rights reserved.
//

import UIKit

class MyItineraryViewController: UIViewController {

    @IBOutlet weak var myItineraryTableView: UITableView!
    @IBOutlet weak var addTripButton: UIBarButtonItem!
    var trips: [Trip] = []
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //View trips from file
        if TripLoader.sharedLoader.readTripsFromFile() != [] {
            self.trips = TripLoader.sharedLoader.readTripsFromFile()
        } else {
        // Add in a refreshControl and load trips from API
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(self.refreshTrips), forControlEvents: UIControlEvents.ValueChanged)
        self.myItineraryTableView.addSubview(self.refreshControl)
        self.refreshTrips()
        }
        
        // Do any additional setup after loading the view.
        self.myItineraryTableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        //When user clicks a trip, show Trip Itinerary VC
        if segue.identifier == "showTripItineraryVC" {
            let destVC: TripItineraryViewController = segue.destinationViewController as! TripItineraryViewController
            
            if let selectedIndexPath = self.myItineraryTableView.indexPathForSelectedRow {
                let trip: Trip = self.trips[selectedIndexPath.row]
                destVC.trip = trip
            }
        }
        
        //When user clicks on Add Trip, show Add Trip VC
        if segue.identifier == "showAddTripVC" {
            let destVC: AddTripViewController = segue.destinationViewController as! AddTripViewController
            
            //Using blocks, if there is a new trip created within Add Trip VC, pass the trip back to My Itinerary VC
            destVC.tripAddedBlock = {(trip: Trip) -> Void in
                
                //Create the days in the trip
                var currentDate: NSDate = trip.startDate
                while currentDate.compare(trip.endDate) != .OrderedDescending {
                    trip.days += [Day(date: currentDate)]
                    currentDate = self.addOneDayAndResetDay(currentDate)
                }

                for day in trip.days {
                    var startTime: NSDate = day.date
                    var startTimeInHour: Int = self.convertNSDateToHour(startTime)
                        
                    while startTimeInHour <= 19 && startTime.compare(trip.endDate) != .OrderedDescending{
                        if startTimeInHour >= 12 && startTimeInHour < 14 {
                            let itinerary: Itinerary = Itinerary(attraction: Attraction(name: "Lunch"), startTime: startTime)
                            day.itineraries.append(itinerary)
                        } else {
                        let itinerary: Itinerary = Itinerary(attraction: trip.chooseRandomAttractions(), startTime: startTime)
                            day.itineraries.append(itinerary)
                        }
                        startTime = NSCalendar.currentCalendar().dateByAddingUnit(NSCalendarUnit.Hour, value: 2, toDate: startTime, options: [])!
                        startTimeInHour = self.convertNSDateToHour(startTime)
                    }
                }
                
                self.trips = [trip] + self.trips
                
                //Upon trip creation, save trips to file
                TripLoader.sharedLoader.saveTripsToFile(self.trips)
                let filePath: NSURL = TripLoader.sharedLoader.dataFileURL()
                print("File path \(filePath.path!)")
                
                //Upon trip creation, save trip to server
                TripLoader.sharedLoader.saveTripToAPI(trip)
                
                //Dismiss AddTripVC
                self.dismissViewControllerAnimated(true, completion: nil)
                self.myItineraryTableView.reloadData()

                //Upon dismissing AddTripVC, instantiate TripItineraryVC and immediately push TripItineraryVC with the trip itinerary without animation
                let tripItineraryVC: TripItineraryViewController = self.storyboard?.instantiateViewControllerWithIdentifier("TripItineraryViewController") as! TripItineraryViewController
                
                tripItineraryVC.trip = trip
                self.navigationController?.pushViewController(tripItineraryVC, animated: false)
            }
        }
    }
    
    func refreshTrips() {
        if let refreshControl = self.refreshControl {
            refreshControl.beginRefreshing()
        }

        var loadTripsFromAPI: NSURLSessionDataTask?
        loadTripsFromAPI = TripLoader.sharedLoader.loadTripsFromAPI({ (trips, error) in
            self.refreshControl.endRefreshing()
            
            if let error = error {
                let alertController: UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            }
            self.trips = trips
            self.myItineraryTableView.reloadData()
        })
        loadTripsFromAPI?.resume()
    }
    
    func convertNSDateToHour(date: NSDate) -> Int {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let timeString: String = dateFormatter.stringFromDate(date)
        let timeInArray: [String] = timeString.componentsSeparatedByString(":")
        let timeHour: Int = Int(timeInArray[0])!
        return timeHour
    }
    
    func addOneDayAndResetDay(date: NSDate) -> NSDate {
        var date: NSDate = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: 1, toDate: date, options: [])!
        date = NSCalendar.currentCalendar().dateBySettingHour(9, minute: 0, second: 0, ofDate: date, options: [])!
        return date
    }
}

extension MyItineraryViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.trips.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //returned from create new view controller
        let cell: MyItineraryCell = tableView.dequeueReusableCellWithIdentifier("MyItineraryCell", forIndexPath: indexPath) as! MyItineraryCell
        let trip: Trip = self.trips[indexPath.row]
        cell.trip = trip
        
        return cell
    }
}