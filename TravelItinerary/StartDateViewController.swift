//
//  DatesViewController.swift
//  TravelItinerary
//
//  Created by Wong Ding Jian on 19/07/2016.
//  Copyright © 2016 ShirleyChew. All rights reserved.
//

import UIKit

class StartDateViewController: UIViewController {

    @IBOutlet weak var startDatePicker: UIDatePicker!
    @IBOutlet weak var doneBarButton: UIBarButtonItem!
    var selectedStartDate: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startDatePickerAction(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        selectedStartDate = dateFormatter.stringFromDate(startDatePicker.date)
        print(selectedStartDate)
    }

    
//    @IBAction func doneBarButtonAction(sender: AnyObject) {
//    
//
//        let initialVC: InitialViewController = self.storyboard?.instantiateViewControllerWithIdentifier("InitialViewController") as! InitialViewController
//        
//        initialVC.mainPageSelected[0] = "Start Date: \(selectedStartDate)"
//        self.navigationController?.pushViewController(initialVC, animated: true)
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
