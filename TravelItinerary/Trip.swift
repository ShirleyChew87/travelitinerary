//
//  Trip.swift
//  TravelItinerary
//
//  Created by Wong Ding Jian on 19/07/2016.
//  Copyright © 2016 ShirleyChew. All rights reserved.
//

import Foundation

class Trip: NSObject, NSCoding {
    var objectId: String?
    var country: String!
    var city: String!
    var startDate: NSDate!
    var endDate: NSDate!
    var days: [Day] = []
    var cityAttractions: [Attraction] = []
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        keyedArchiver.encodeObject(self.country, forKey: "MTITripCountry")
        keyedArchiver.encodeObject(self.city, forKey: "MTITripCity")
        keyedArchiver.encodeObject(self.startDate, forKey: "MTITripStartDate")
        keyedArchiver.encodeObject(self.endDate, forKey: "MTITripEndDate")
        keyedArchiver.encodeObject(self.days, forKey: "MTITripDays")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        // Read saved data
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        
        let country: String = keyedUnarchiver.decodeObjectForKey("MTITripCountry") as! String
        let city: String = keyedUnarchiver.decodeObjectForKey("MTITripCity") as! String
        let startDate: NSDate = keyedUnarchiver.decodeObjectForKey("MTITripStartDate") as! NSDate
        let endDate: NSDate = keyedUnarchiver.decodeObjectForKey("MTITripEndDate") as! NSDate
        
        // Create a new Trip instance, passing it the saved data
        self.init(country: country, city: city, startDate: startDate, endDate: endDate)
        
        // Load saved itineraries to the new instance
        if let savedDays: [Day] = keyedUnarchiver.decodeObjectForKey("MTITripDays") as? [Day] {
            self.days += savedDays
        }
    }
    
    init(country: String, city: String, startDate: NSDate?, endDate: NSDate?) {
        self.country = country
        self.city = city
        self.startDate = startDate
        self.endDate = endDate
        
        super.init()
    }
    
    func chooseRandomAttractions() -> Attraction {
        let index: Int = Int(arc4random_uniform(UInt32(cityAttractions.count)))
        return cityAttractions[index]
    }
}

extension Trip {
    convenience init?(objectId: String, json: [String : AnyObject]) {
        // Get values from json dictionary
        let country: String = json["country"] as! String
        let city: String = json["city"] as! String
        let startDate: NSDate = json["startDate"] as! NSDate
        let endDate: NSDate = json["endDate"] as! NSDate
        self.init(country: country, city: city, startDate: startDate, endDate: endDate)
        
        updateWithJson(json)
        self.objectId = objectId
    }
    
    func updateWithJson(json: [String : AnyObject]) {
        if let country = json["country"] as? String {
            self.country = country
        }
        if let city = json["city"] as? String {
            self.city = city
        }
        if let startDate = json["startDate"] as? NSDate {
            self.startDate = startDate
        }
        if let endDate = json["endDate"] as? NSDate {
            self.endDate = endDate
        }
    }
    
    func jsonDict() -> [String : AnyObject] {
        let jsonDict: [String : AnyObject] = [
            "country": self.country,
            "city": self.city,
            "startDate": self.startDate,
            "endDate": self.endDate,
            ]
        
        return jsonDict
    }
}