//
//  TripItineraryCell.swift
//  TravelItinerary
//
//  Created by Wong Ding Jian on 02/08/2016.
//  Copyright © 2016 ShirleyChew. All rights reserved.
//

import UIKit

class TripItineraryCell: UITableViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    
    var itinerary: Itinerary! {
        didSet {
            updateDisplay()
        }
    }
    
    func updateDisplay() {
        self.nameLabel.text = itinerary.attraction.name
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let startTimeString = dateFormatter.stringFromDate(itinerary.startTime)
        self.startTimeLabel.text = startTimeString
        
        //If there is photo attached to attraction, load that. Else, check for photo on API and load.
        if let photo = itinerary.attraction.photo {
            self.photoImageView.image = photo
            
        } else {
            if let photoURL = itinerary.attraction.photoURLString {
                var loadAttractionImageTask: NSURLSessionDataTask?
                loadAttractionImageTask = TripLoader.sharedLoader.loadAttractionImageFromAPI(photoURL, completionBlock: {(photo) in
                    self.itinerary.attraction.photo = photo
                    self.photoImageView.image = photo
                })
                loadAttractionImageTask?.resume()
            }
        }
    }
}
