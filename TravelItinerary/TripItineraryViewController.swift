//
//  TripItineraryViewController.swift
//  TravelItinerary
//
//  Created by Wong Ding Jian on 21/07/2016.
//  Copyright © 2016 ShirleyChew. All rights reserved.
//

import UIKit

class TripItineraryViewController: UIViewController {
   
    var trip: Trip!
    @IBOutlet weak var tripItineraryTableView: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //Populate Date array with each day of travel
        self.tripItineraryTableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showAttractionInfoVC" {
            let destVC: AttractionInformationViewController = segue.destinationViewController as! AttractionInformationViewController
            
            if let selectedIndexPath = self.tripItineraryTableView.indexPathForSelectedRow {
                let itinerary: Itinerary = self.trip.days[selectedIndexPath.section].itineraries[selectedIndexPath.row]
                destVC.attraction = itinerary.attraction
            }
        }
    }
}

extension TripItineraryViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return trip.days.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE, MMMM dd, yyyy"

        return dateFormatter.stringFromDate(trip.days[section].date)
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trip.days[section].itineraries.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: TripItineraryCell = tableView.dequeueReusableCellWithIdentifier("TripItineraryCell", forIndexPath: indexPath) as! TripItineraryCell
        let itinerary: Itinerary = trip.days[indexPath.section].itineraries[indexPath.row]
        
        cell.itinerary = itinerary
    
        return cell
    }
}
