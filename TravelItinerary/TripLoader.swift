//
//  TripLoader.swift
//  TravelItinerary
//
//  Created by Wong Ding Jian on 02/08/2016.
//  Copyright © 2016 ShirleyChew. All rights reserved.
//

import Foundation
import UIKit

class TripLoader {
    //Singleton pattern
    static let sharedLoader: TripLoader = TripLoader()
    var session: NSURLSession!
    
    private init() {
    let config: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
    config.HTTPAdditionalHeaders = ["application-id" : "8CC4139B-7564-65C2-FFA5-9A272633CB00", "secret-key" : "AE3D0CDB-A45F-9A69-FF4E-BF40A1BBD300"]
    self.session = NSURLSession(configuration: config)
    }
    
    //Manage data files
    let fileManager: NSFileManager = NSFileManager.defaultManager()
    
    func dataFileURL() -> NSURL {
        let documentsDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        let filePath: NSURL = documentsDirectory.URLByAppendingPathComponent("data.plist")
        
        return filePath
    }
    
    func readTripsFromFile() -> [Trip] {
        let tripsData: [NSData]? = NSArray(contentsOfURL: self.dataFileURL()) as? [NSData]
        var trips: [Trip] = []
        if let tripsData = tripsData {
            for data in tripsData {
                let trip: Trip = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Trip
                trips.append(trip)
            }
        }
        return trips
    }
    
    func saveTripsToFile(trips: [Trip]) {
        var tripsData: [NSData] = []
        for trip in trips {
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(trip)
            tripsData.append(data)
        }
        (tripsData as NSArray).writeToURL(self.dataFileURL(), atomically: true)
    }
    
    //Manage loading from API
    func readCountryFromAPI(completionBlock:((availableCountries: [String], availableCities: [String])->Void)?) -> NSURLSessionDataTask {
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/data/Countries")!
        let loadTask: NSURLSessionDataTask = self.session.dataTaskWithURL(url) { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            
            if let json: [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: []) as? [String : AnyObject] {
                var availableCountries: [String] = []
                var availableCities: [String] = []
                
                if let countryArray: [ [String : AnyObject] ] = json["data"] as? [ [String : AnyObject] ] {
                    for countryData in countryArray {
                        if let country: String = countryData["country"] as? String {
                            availableCountries.append(country)
                        }
                        if let city: String = countryData["city"] as? String {
                            availableCities.append(city)
                        }
                    }
                }
                dispatch_async(dispatch_get_main_queue(), {() -> Void in
                    completionBlock?(availableCountries: availableCountries, availableCities: availableCities)
                })
            }
        }
        return loadTask
    }
    
    func readAttractionsFromAPI(country: String, city: String, completionBlock:((availableAttractions: [Attraction])->Void)?) -> NSURLSessionDataTask {
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/data/Attractions?where=country%20%3D%20%27\(country)%27%20AND%20city%20%3D%20%27\(city)%27")!
        let loadTask: NSURLSessionDataTask = self.session.dataTaskWithURL(url) { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            if let json: [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: []) as? [String : AnyObject] {
                
                var availableAttractions: [Attraction] = []
                if let attractionArray: [ [String : AnyObject] ] = json["data"] as? [ [String : AnyObject] ] {
                    for attractionData in attractionArray {
                        if let name: String = attractionData["name"] as? String {
                            let attraction: Attraction = Attraction(name: name)
                            if let country: String = attractionData["country"] as? String {
                                attraction.country = country
                            }
                            if let city: String = attractionData["city"] as? String {
                                attraction.city = city
                            }
                            if let address: String = attractionData["address"] as? String {
                                attraction.address = address
                            }
                            if let writeUp: String = attractionData["writeUp"] as? String {
                                attraction.writeUp = writeUp
                            }
                            if let openingHours: String = attractionData["openingHours"] as? String {
                                attraction.openingHours = openingHours
                            }
                            if let fee: String = attractionData["fee"] as? String {
                                attraction.fee = fee
                            }
                            if let reviews: String = attractionData["reviews"] as? String {
                                attraction.reviews = reviews
                            }
                            if let fileURL: String = attractionData["photo"] as? String {
                                attraction.photoURLString = fileURL
                            }
                            if let objectId: String = attractionData["objectId"] as? String {
                                attraction.id = objectId
                            }
                            availableAttractions.append(attraction)
                        }
                    }
                }
                dispatch_async(dispatch_get_main_queue(), {() -> Void in
                    completionBlock?(availableAttractions: availableAttractions)
                })
            }
        }
        return loadTask
    }
    
    func loadAttractionImageFromAPI(photoURLString: String, completionBlock: ((attractionImage: UIImage) -> Void)?) -> NSURLSessionDataTask {
        let photoURL: NSURL = NSURL(string: photoURLString)!
        let loadTask: NSURLSessionDataTask = self.session.dataTaskWithURL(photoURL) { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            let photo: UIImage = UIImage(data: data!)!
            dispatch_async(dispatch_get_main_queue(), {() -> Void in
                completionBlock?(attractionImage: photo)
            })
        }
        return loadTask
    }
    
    func saveTripToAPI(trip: Trip) {
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/data/Trips")!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        urlRequest.HTTPMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")

        var dayDictArray: [[String : AnyObject]] = []
        for day in trip.days {
            dayDictArray.append(day.jsonDict())
        }
        
        let jsonDict: [String : AnyObject] = [
            "city" : trip.city,
            "country" : trip.country,
            "startDate" : convertNSDateToString(trip.startDate),
            "endDate" : convertNSDateToString(trip.endDate),
            "days" : dayDictArray
        ]
        
        let jsonData: NSData = try! NSJSONSerialization.dataWithJSONObject(jsonDict, options: [])
        urlRequest.HTTPBody = jsonData
        
        let postTask: NSURLSessionDataTask = self.session.dataTaskWithRequest(urlRequest) { (data: NSData?, response: NSURLResponse?, error:NSError?) in
            let httpCode = (response as! NSHTTPURLResponse).statusCode
            let jsonString: String? = String(data: data!, encoding: NSUTF8StringEncoding)
            print("Server code \(httpCode): \(jsonString)")
            print("Trip uploaded to API")
        }
        postTask.resume()
    }
    
    func loadTripsFromAPI(completionBlock:((trips: [Trip], error: NSError?) -> Void)?) -> NSURLSessionDataTask {
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/data/Trips?loadRelations=days.itineraries.attraction1")!
        let loadTask: NSURLSessionDataTask = self.session.dataTaskWithURL(url) { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            
            if let error = error {
                completionBlock?(trips: [], error: error)
            } else {
            if let json: [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: []) as? [String : AnyObject]{
                var trips: [Trip] = []
                if let tripsArray: [ [String : AnyObject] ] = json["data"] as? [ [String: AnyObject] ] {
                    for tripData in tripsArray {
                        if let country: String = tripData["country"] as? String {
                            if let city: String = tripData["city"] as? String {
                                if let startDate: String = tripData["startDate"] as? String {
                                    if let endDate: String = tripData["endDate"] as? String {
                                        //Create Trip instance
                                        let trip: Trip = Trip(country: country, city: city, startDate: self.convertStringToNSDate(startDate), endDate: self.convertStringToNSDate(endDate))
                                        if let daysArray: [ [String : AnyObject] ] = tripData["days"] as? [ [String : AnyObject] ]{
                                            for dayData in daysArray {
                                                if let date: String = dayData["dayDate"] as? String {
                                                    //Create Day instance
                                                    let day: Day = Day(date: self.convertStringToNSDate(date))
                                                    if let itinerariesArray: [ [String : AnyObject] ] = dayData["itineraries"] as? [ [String : AnyObject] ] {
                                                        for itineraryData in itinerariesArray {
                                                            if let startTime: String = itineraryData["startTime"] as? String {
                                                                if let attractionData: [String : AnyObject] = itineraryData["attraction1"] as? [String : AnyObject] {
                                                                    if let name: String = attractionData["name"] as? String {
                                                                        //Create Attraction instance
                                                                        let attraction: Attraction = Attraction(name: name)
                                                                        if let country: String = attractionData["country"] as? String {
                                                                            attraction.country = country
                                                                        }
                                                                        if let city: String = attractionData["city"] as? String {
                                                                            attraction.city = city
                                                                        }
                                                                        if let address: String = attractionData["address"] as? String {
                                                                            attraction.address = address
                                                                        }
                                                                        if let writeUp: String = attractionData["writeUp"] as? String {
                                                                            attraction.writeUp = writeUp
                                                                        }
                                                                        if let openingHours: String = attractionData["openingHours"] as? String {
                                                                            attraction.openingHours = openingHours
                                                                        }
                                                                        if let fee: String = attractionData["fee"] as? String {
                                                                            attraction.fee = fee
                                                                        }
                                                                        if let reviews: String = attractionData["reviews"] as? String {
                                                                            attraction.reviews = reviews
                                                                        }
                                                                        if let fileURL: String = attractionData["photo"] as? String {
                                                                            attraction.photoURLString = fileURL
                                                                        }
                                                                        if let objectId: String = attractionData["objectId"] as? String {
                                                                            attraction.id = objectId
                                                                        }

                                                                        //Create Itinerary instance and appending it to Day itineraries
                                                                        let itinerary: Itinerary = Itinerary(attraction: attraction, startTime: self.convertStringToNSDate(startTime))
                                                                        day.itineraries.append(itinerary)
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    trip.days.append(day)
                                                }
                                            }
                                        }
                                        trips.append(trip)
                                    }
                                }
                            }
                        }
                    }
                }
                dispatch_async(dispatch_get_main_queue(), {() -> Void in
                    completionBlock?(trips: trips, error: nil)
                })
            }
            }
        }

        return loadTask
    }
    
    func convertNSDateToString(date: NSDate) -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE, MMMM dd, yyyy HH:mm"
        
        return dateFormatter.stringFromDate(date)
    }
    
    func convertStringToNSDate(dateString: String) -> NSDate? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE, MMMM dd, yyyy HH:mm"
        
        if let date = dateFormatter.dateFromString(dateString) {
            return date
        }

        return nil
    }

}