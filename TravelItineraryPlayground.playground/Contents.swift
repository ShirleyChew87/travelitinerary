//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
var queryString = "Country = 'Italy' AND City = 'Rome'"
let properlyFormatterParam = queryString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet())
print(properlyFormatterParam)