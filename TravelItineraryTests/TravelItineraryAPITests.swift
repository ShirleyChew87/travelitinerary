//
//  TravelItineraryAPITests.swift
//  TravelItinerary
//
//  Created by Wong Ding Jian on 05/08/2016.
//  Copyright © 2016 ShirleyChew. All rights reserved.
//

import XCTest
@testable import TravelItinerary

class TravelItineraryAPITests: XCTestCase {
    
    var session: NSURLSession!
    var availableCountries: [String] = []
    var availableCities: [String] = []
//    let baseURL: NSURL = NSURL(string: "https://api.backendless.com/v1/")!
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let config: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        config.HTTPAdditionalHeaders = ["application-id" : "8CC4139B-7564-65C2-FFA5-9A272633CB00", "secret-key" : "AE3D0CDB-A45F-9A69-FF4E-BF40A1BBD300"]
        self.session = NSURLSession(configuration: config)        
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()

    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testCreatingTripOnAPI() {
        let expectation = self.expectationWithDescription("Create Task")
        
        let config: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        config.HTTPAdditionalHeaders = ["application-id" : "8CC4139B-7564-65C2-FFA5-9A272633CB00", "secret-key" : "AE3D0CDB-A45F-9A69-FF4E-BF40A1BBD300", "Content-Type": "application/json"]
        self.session = NSURLSession(configuration: config)
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/data/Trips")!
        
        let trip: Trip = Trip(country: "Malaysia", city: "Johor", startDate: NSDate(), endDate: NSDate())
        
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        urlRequest.HTTPMethod = "POST"
        
        let jsonDict: [String : AnyObject] = ["country": trip.country, "city": trip.city]//, "startDate": trip.startDate, "endDate": trip.endDate]
        let jsonData: NSData = try! NSJSONSerialization.dataWithJSONObject(jsonDict, options: NSJSONWritingOptions(rawValue: 0))
        
        urlRequest.HTTPBody = jsonData
        
        let postTask: NSURLSessionDataTask = self.session.dataTaskWithRequest(urlRequest) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            // code
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            XCTAssert(httpCode == 200, "Should have returned 200 Content OK")
            let jsonString: String? = String(data: data!, encoding: NSUTF8StringEncoding)
            let json : [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)) as! [String : AnyObject]
            trip.objectId = json["objectId"] as? String
            NSLog("HTTP \(httpCode): \(jsonString!)")
            expectation.fulfill()
        }
        
        postTask.resume()
        self.waitForExpectationsWithTimeout(10, handler: nil)
    }

    func testLoadCountryFromAPI() {
        let expectation = self.expectationWithDescription("Load task")
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/data/Countries")!
        let loadTask: NSURLSessionDataTask = self.session.dataTaskWithURL(url) { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            XCTAssert(httpCode == 200, "Should have returned 200 Content OK")
            
            let json: [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String : AnyObject]
            
            if let countryArray: [ [String : AnyObject] ] = json["data"] as? [ [String : AnyObject] ] {
                for countryData in countryArray {
                    if let country: String = countryData["country"] as? String {
                        self.availableCountries.append(country)
                    }
                    if let city: String = countryData["city"] as? String {
                        self.availableCities.append(city)
                    }
                }
                print(self.availableCountries)
                print(self.availableCities)
                let cities: String = self.availableCities[0]
                let citiesArray: [String] = cities.componentsSeparatedByString(",")
                print(citiesArray)
            }
            NSLog("Task Completed")
            expectation.fulfill()
        }
        loadTask.resume()
        self.waitForExpectationsWithTimeout(30, handler: nil)
    }
    
    func testLoadAttractionFromAPI() {
        let expectation = self.expectationWithDescription("Load task")
        let queryString = "country = 'Italy' AND city = 'Rome'"
        let properlyFormatterParam = queryString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet())
        print(properlyFormatterParam!)
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/data/Attractions?where=\(properlyFormatterParam!)")!
        let loadTask: NSURLSessionDataTask = self.session.dataTaskWithURL(url) { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            XCTAssert(httpCode == 200, "Should have returned 200 Content OK")
            
            let json: [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String : AnyObject]
            
            if let attractionArray: [ [String : AnyObject] ] = json["data"] as? [ [String : AnyObject] ] {
                for attractionData in attractionArray {
                    if let name: String = attractionData["name"] as? String {
                        print(name)
                    }
                }
            }
            NSLog("Task Completed")
            expectation.fulfill()
        }
        loadTask.resume()
        self.waitForExpectationsWithTimeout(30, handler: nil)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testCreateAttractionImageOnServer() {
        let expectation = self.expectationWithDescription("Create Image")
        
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/files/attractions/photo.jpg")!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        urlRequest.HTTPMethod = "POST"
        
        let uuid: String = NSUUID().UUIDString
        urlRequest.addValue("multipart/form-data; boundary=\"_ATTRACTIONS_BOUNDARY_\"", forHTTPHeaderField: "Content-Type")
        let boundaryHeader: NSMutableString = NSMutableString()
        boundaryHeader.appendString("--_ATTRACTIONS_BOUNDARY_\n")
        boundaryHeader.appendString("Content-Disposition: form-data; name=\"file\"; filename=\(uuid)\"\n")
        boundaryHeader.appendString("Content-Type: image/jpg\n\n")
        
        let bodyData: NSMutableData = NSMutableData()
        bodyData.appendData(boundaryHeader.dataUsingEncoding(NSASCIIStringEncoding)!)
        
        let attraction: Attraction = Attraction(name: "Pantheon")
        let imageData: NSData = UIImageJPEGRepresentation(attraction.photo!, 0.9)!
        bodyData.appendData(imageData)
        
        let uploadTask: NSURLSessionUploadTask = self.session.uploadTaskWithRequest(urlRequest, fromData: bodyData) { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            XCTAssert(httpCode == 201, "Should have returned 200 Content OK")
            let jsonString: String? = String(data: data!, encoding: NSUTF8StringEncoding)
            NSLog("Server code \(httpCode): \(jsonString)")
            expectation.fulfill()
        }
        uploadTask.resume()
        self.waitForExpectationsWithTimeout(30, handler: nil)
    }
    
    func testCreateAttractionOnServer() {
        let expectation = self.expectationWithDescription("Create Attraction")
        let fileURL: String = "https://api.backendless.com/8cc4139b-7564-65c2-ffa5-9a272633cb00/v1/files/attractions/photo.jpg"
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/data/Attractions")!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        urlRequest.HTTPMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let attraction: Attraction = Attraction(name: "Colosseum")
        let jsonDict: [String : AnyObject] = [
        "name": attraction.name,
        "country": "Hello",
        "city": "Hello",
        "address": "Hello",
        "writeUp": "Hello",
        "openingHours": "Hello",
        "fee": "Hello",
        "reviews": "Hello",
        "photo": fileURL
        ]
        
        let jsonData: NSData = try! NSJSONSerialization.dataWithJSONObject(jsonDict, options: NSJSONWritingOptions(rawValue: 0))
        urlRequest.HTTPBody = jsonData
        
        let postData: NSURLSessionDataTask = self.session.dataTaskWithRequest(urlRequest) {(data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            XCTAssert(httpCode == 201, "Should have returned 200 Content OK")
            let jsonString: String? = String(data: data!, encoding: NSUTF8StringEncoding)
            NSLog("Server code \(httpCode): \(jsonString)")
            expectation.fulfill()
        }
        postData.resume()
        self.waitForExpectationsWithTimeout(30, handler: nil)
    }
    
    func testCreateTestName() {
        let expectation = self.expectationWithDescription("Name")
        
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/data/Testing")!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        urlRequest.HTTPMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let jsonDict: [String : AnyObject] = ["name" : "Shirley"]
        let jsonData: NSData = try! NSJSONSerialization.dataWithJSONObject(jsonDict, options: NSJSONWritingOptions(rawValue: 0))
        urlRequest.HTTPBody = jsonData
        
        let postTask: NSURLSessionDataTask = self.session.dataTaskWithRequest(urlRequest) { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            XCTAssert(httpCode == 200, "Should have returned 201 Content OK")
            expectation.fulfill()
        }
        postTask.resume()
        self.waitForExpectationsWithTimeout(30, handler: nil)
    }
    
    func testCreateTrip() {
        let expectation = expectationWithDescription("Load trip")
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/data/Trips")!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        urlRequest.HTTPMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let startDate: NSDate = NSDate()
        let endDate: NSDate = NSCalendar.currentCalendar().dateByAddingUnit(NSCalendarUnit.Day, value: 1, toDate: startDate, options: [])!
        let trip: Trip = Trip(country: "Italy", city: "Rome", startDate: startDate, endDate: endDate)
        
        var currentDate: NSDate = trip.startDate
        var dayNumber: Int = 1
        while currentDate.compare(trip.endDate) != .OrderedDescending {
            trip.days += [Day(date: currentDate)]
            currentDate = addOneDayAndResetDay(currentDate)
            dayNumber += 1
        }
        
        for day in trip.days {
            var startTime: NSDate = day.date
            var startTimeInHour: Int = convertNSDateToHour(startTime)
            
            while startTimeInHour <= 19 && startTime.compare(trip.endDate) != .OrderedDescending{
                if startTimeInHour >= 12 && startTimeInHour < 14 {
                    let itinerary: Itinerary = Itinerary(attraction: Attraction(name: "Lunch"), startTime: startTime)
                    day.itineraries.append(itinerary)
                } else {
                    let itinerary: Itinerary = Itinerary(attraction: Attraction(name: "St Peter's Basilica"), startTime: startTime)
                    day.itineraries.append(itinerary)
                }
                startTime = NSCalendar.currentCalendar().dateByAddingUnit(NSCalendarUnit.Hour, value: 2, toDate: startTime, options: [])!
                startTimeInHour = self.convertNSDateToHour(startTime)
            }
        }
        
        var attractionDict: [String : AnyObject] = [:]
        var itineraryDict: [String : AnyObject] = [:]
        var itineraryDictArray: [[String : AnyObject]] = []
        var dayDict: [String : AnyObject] = [:]
        var dayDictArray: [[String : AnyObject]] = []
        
        for day in trip.days {
            
            for itinerary in day.itineraries {
                let attraction: Attraction = itinerary.attraction
                attractionDict["___class"] = "Attractions"
                attractionDict["name"] = attraction.name
                attractionDict["country"] = attraction.country
                attractionDict["city"] = attraction.city
                attractionDict["address"] = attraction.address
                attractionDict["writeUp"] = attraction.writeUp
                attractionDict["openingHours"] = attraction.openingHours
                attractionDict["fee"] = attraction.fee
                attractionDict["photo"] = attraction.photoURLString
                attractionDict["reviews"] = attraction.reviews
                
            itineraryDict = [
                "___class": "Itineraries",
                "startTime" : convertNSDateToString(itinerary.startTime),
                "endTime" : NSNull(),
                "attraction1" : attractionDict
                ]
            itineraryDictArray.append(itineraryDict)
            }
            dayDict = [
            "___class": "Days",
            "dayDate" : convertNSDateToString(day.date),
            "itineraries" : itineraryDictArray
            ]
            dayDictArray.append(dayDict)
        }
        
        let jsonDict: [String : AnyObject] = [
            "city" : trip.city,
            "country" : trip.country,
            "startDate" : convertNSDateToString(trip.startDate),
            "endDate" : convertNSDateToString(trip.endDate),
            "days" : dayDictArray
        ]

        
        let jsonData: NSData = try! NSJSONSerialization.dataWithJSONObject(jsonDict, options: [])
        urlRequest.HTTPBody = jsonData
        
        let postTask: NSURLSessionDataTask = self.session.dataTaskWithRequest(urlRequest) { (data: NSData?, response: NSURLResponse?, error:NSError?) in
            let httpCode = (response as! NSHTTPURLResponse).statusCode
            XCTAssert(httpCode == 200, "Should have returned 200 Content OK")
            let jsonString: String? = String(data: data!, encoding: NSUTF8StringEncoding)
            
            let json: [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String : AnyObject]
            if let jsonArray: [[String : AnyObject]] = json["data"] as? [[String : AnyObject]] {
                for jsonDataRead in jsonArray {
                    if let objectID: String = jsonDataRead["objectId"] as? String{
                        print("The object ID for the trip uploaded is \(objectID)")
                    }
                }
            }
            
            NSLog("Server code \(httpCode): \(jsonString)")
            expectation.fulfill()
        }
        postTask.resume()
        self.waitForExpectationsWithTimeout(30, handler: nil)
    }
    
    func testLoadTrips() {
        let expectation = self.expectationWithDescription("Load task")
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/data/Trips")!
        let loadTask: NSURLSessionDataTask = self.session.dataTaskWithURL(url) { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            if let json: [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: []) as? [String : AnyObject]{
                if let tripsArray: [ [String : AnyObject] ] = json["data"] as? [ [String : AnyObject] ] {
                    for tripData in tripsArray {
                        if let daysArray: [ [String : AnyObject] ] = tripData["days"] as? [ [String : AnyObject] ] {
                            for dayData in daysArray {
                                if let dayDate: String = dayData["dayDate"] as? String {
                                    print(dayDate)
                                }
                            }
                        }
                    }
                    print(tripsArray)
                }
            }
            expectation.fulfill()
        }
        loadTask.resume()
        self.waitForExpectationsWithTimeout(60, handler: nil)
    }
    
    func convertNSDateToHour(date: NSDate) -> Int {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let timeString: String = dateFormatter.stringFromDate(date)
        let timeInArray: [String] = timeString.componentsSeparatedByString(":")
        let timeHour: Int = Int(timeInArray[0])!
        return timeHour
    }
    
    func addOneDayAndResetDay(date: NSDate) -> NSDate {
        var date: NSDate = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: 1, toDate: date, options: [])!
        date = NSCalendar.currentCalendar().dateBySettingHour(9, minute: 0, second: 0, ofDate: date, options: [])!
        return date
    }
    
    func convertNSDateToString(date: NSDate) -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE, MMMM dd, yyyy HH:mm"
        
        return dateFormatter.stringFromDate(date)
    }
    
    func convertStringToNSDate(date: String) -> NSDate {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE, MMMM dd, yyyy HH:mm"
        
        return dateFormatter.dateFromString(date)!
    }
}
