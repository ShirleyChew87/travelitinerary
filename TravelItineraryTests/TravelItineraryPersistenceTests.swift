//
//  TravelItineraryPersistenceTests.swift
//  TravelItinerary
//
//  Created by Wong Ding Jian on 28/07/2016.
//  Copyright © 2016 ShirleyChew. All rights reserved.
//

import XCTest
@testable import TravelItinerary

class TravelItineraryPersistenceTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testWritingData() {
        var trips: [Trip] = []
        trips.append(Trip(country: "Italy", city: "Rome", startDate: NSDate(), endDate: NSDate()))
        trips.append(Trip(country: "Malaysia", city: "Kuala Lumpur", startDate: NSDate(), endDate: NSDate()))
        
        TripLoader.sharedLoader.saveTripsToFile(trips)
        let filePath: NSURL = TripLoader.sharedLoader.dataFileURL()
        
        var fileExists: Bool = NSFileManager.defaultManager().fileExistsAtPath(filePath.path!)
        XCTAssert(fileExists, "File should exist")
        
        NSLog("File path \(filePath.path!)")
    }
    
    func testReadingFromFile() {
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        let documentsDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        let filePath: NSURL = documentsDirectory.URLByAppendingPathComponent("data.plist")
        
        let tripsData: [NSData] = NSArray(contentsOfURL: filePath) as! [NSData]
        var trips: [Trip] = []
        
        for data in tripsData {
            let trip: Trip = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Trip
            trips.append(trip)
        }
        XCTAssert(trips.count == 2, "Trip count should be 2")
    }
    
    func testSavingTrip() {
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let trip: Trip = Trip(country: "Italy", city: "Rome", startDate: NSDate(), endDate: NSDate())
        let tripData: NSData = NSKeyedArchiver.archivedDataWithRootObject(trip)
        defaults.setObject(tripData, forKey: "testTripData")
        defaults.synchronize()
        
        let readTripData: NSData = defaults.objectForKey("testTripData") as! NSData
        let readTrip: Trip? = NSKeyedUnarchiver.unarchiveObjectWithData(readTripData) as? Trip
        
        XCTAssertNotNil(readTrip, "Should be able to read Trip")
        XCTAssertEqual(trip.city, readTrip!.city, "Names of trip and readTrip should match")
        
    }
    
    func testReadingArray() {
        
    }
    
    func testReadingFromDefaults() {
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let firstValue: String? = defaults.stringForKey("FirstKey")
        
        XCTAssert(firstValue != nil , "firstValue is not nil")
        
        defaults.setObject("Shirley", forKey: "FirstKey")
        defaults.synchronize()
        
        let name: String? = defaults.stringForKey("FirstKey")
        
        XCTAssert(name == "Shirley", "Name doesn't match")
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
