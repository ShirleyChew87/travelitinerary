//
//  TravelItineraryTests.swift
//  TravelItineraryTests
//
//  Created by Wong Ding Jian on 16/07/2016.
//  Copyright © 2016 ShirleyChew. All rights reserved.
//

import XCTest
@testable import TravelItinerary

class TravelItineraryTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testGetCharactersInString() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let startTimeString: String = dateFormatter.stringFromDate(NSDate())
        let startTimeInArray: [String] = startTimeString.componentsSeparatedByString(":")
        let startTimeHour: Int = Int(startTimeInArray[0])!
        print("The hour is \(startTimeHour)")
    }
    
    func testMoveStartTimeByOneDay() {
        var startTime: NSDate = NSDate()
        startTime = NSCalendar.currentCalendar().dateByAddingUnit(NSCalendarUnit.Day, value: 1, toDate: startTime, options: [])!
        startTime = NSCalendar.currentCalendar().dateBySettingHour(9, minute: 0, second: 0, ofDate: startTime, options: [])!
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE, MMMM dd, yyyy HH:mm"
        let startTimeString: String = dateFormatter.stringFromDate(startTime)
        print("The next day is \(startTimeString)")
    }
    
}
